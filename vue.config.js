module.exports = {
  productionSourceMap: false,
  lintOnSave: false,
  devServer: {
    port:8888,
    proxy: {
      '/api': {
        target: 'http://localhost:8082',
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  },
}
