import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from '../views/login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login
  },
  {
    path: '/index',
    name: 'index',
    redirect: '/checkitem',
    component: () => import('../views/index.vue'),
    children: [
      {
        path: '/checkitem',
        name: 'checkitem',
        component: () => import('../views/appoment/checkitem.vue')
      },
      {
        path: '/checkgroup',
        name: 'checkgroup',
        component: () => import('../views/appoment/checkgroup.vue')
      },
      {
        path: '/ordersetting',
        name: 'ordersetting',
        component: () => import('../views/appoment/ordersetting.vue')
      },
      {
        path: '/setmeal',
        name: 'setmeal',
        component: () => import('../views/appoment/setmeal.vue')
      },
      {
        path: '/report_setmeal',
        name: 'report_setmeal',
        component: () => import('../views/report/report_setmeal.vue')
      },
      {
        path: '/report_sex',
        name: 'report_sex',
        component: () => import('../views/report/report_sex.vue')
      },
      {
        path: '/report_age',
        name: 'report_age',
        component: () => import('../views/report/report_age.vue')},
      {
        
        path: '/report_business',
        name: 'report_business',
        component: () => import('../views/report/report_business.vue')
      },
      {
        path: '/usermanager',
        name: 'usermanager',
        component: () => import('../views/appoment/usermanager.vue')
      },
      {
        path: '/rolemanager',
        name: 'rolemanager',
        component: () => import('../views/appoment/rolemanager.vue')
      },
      {
        path: '/report_setmealDate',
        name: 'report_setmealDate',
        component: () => import('../views/report/report_setmealDate.vue')
      },
      {
        path: '/report_member',
        name: 'report_member',
        component: () => import('../views/report/report_member.vue')
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
