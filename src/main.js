import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)
import axios from 'axios'
import echarts from 'echarts'
Vue.prototype.$echarts = echarts

Vue.config.productionTip = false

Vue.prototype.$http = axios

import moment from "moment";
Vue.prototype.moment = moment;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')


//添加响应拦截器
axios.interceptors.response.use(function(response){
  if(response.data.flag==false && response.data.flag!=undefined){
    ElementUI.Message({
      message:response.data.message,
      type:'error'
    });
    return Promise.reject(response);
  }
  return response;
},function(error){
  ElementUI.Message({
    message:'服务出错，请联系管理员',
    type:'error'
  });
});


// 添加请求拦截器
axios.interceptors.request.use(
  config => {
    if(config.url != 'api/login'){
      config.headers['Authorization'] = window.sessionStorage.getItem('token');
    }
    return config;
  }
);

router.beforeEach((to, from, next) => {
  if(to.name == 'login'){
      next();
  }else{
      if(window.sessionStorage.getItem('token')){
          next();
      }else{
          next({path:"/"});
      }
  }
});